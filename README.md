# w7corp-pre/sdk-mini-vscode示例项目

## 简介
w7corp-pre/sdk-mini-vscode示例项目
## 快速开始
[在线示例](https://vscode.i0358.cn)

```docker
docker run -it -p 9011:9000 ccr.ccs.tencentyun.com/afan-public/mini-vscode:v1.0.3
#docker run -it -p 9011:9000 -e "VSCODE_EDITOR_ROOT=/home/editor" -v ${PWD}:/home/editor ccr.ccs.tencentyun.com/afan-public/mini-vscode:v1.0.3
浏览器打开　http://127.0.0.1:9011
```

### 1. 安装

```bash
git clone https://gitee.com/free-worker/web-vscode-editor.git
composer install 
```

### 2. 启动

```bash
php artisan serve
```

### 3. docker 构建
```bash
docker build --no-cache -t ccr.ccs.tencentyun.com/afan-public/mini-vscode:v1.0.3 .
```



