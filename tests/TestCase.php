<?php

namespace Tests;

use Illuminate\Auth\CreatesUserProviders;

abstract class TestCase extends \Illuminate\Foundation\Testing\TestCase
{
    use CreatesApplication;
}
