FROM phpdockerio/php74-cli:latest
RUN mkdir -p /home/code && mkdir -p /home/editor
ADD . /home/code
WORKDIR /home/code
CMD ["/usr/bin/php", "artisan", "serve", "--host=0.0.0.0", "--port=9000"]


